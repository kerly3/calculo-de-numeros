// Función para realizar los cálculos
function calcular() {
    // Obtener los valores ingresados por el usuario
    var num1 = parseFloat(document.getElementById("num1").value);
    var num2 = parseFloat(document.getElementById("num2").value);
    
    // Div para mostrar los resultados
    var resultadosDiv = document.getElementById("resultados");
    resultadosDiv.innerHTML = ""; // Limpiar resultados anteriores
    
    // Agregar mensaje inicial centrado y en negrilla
    var mensajeInicial = document.createElement("div");
    mensajeInicial.innerHTML = "<strong>Los resultados son:</strong>";
    mensajeInicial.style.textAlign = "center"; // Centrar el texto
    resultadosDiv.appendChild(mensajeInicial);
    
    // Realizar el bucle de 5 iteraciones
    for (var i = 1; i <= 5; i++) {
        var resultado;
        var operacion;

        switch (i) {
            case 1:
                resultado = num1 + num2;
                operacion = "SUMA";
                break;
            case 2:
                resultado = num1 - num2;
                operacion = "RESTA";
                break;
            case 3:
                resultado = num1 * num2;
                operacion = "MULTIPLICACIÓN";
                break;
            case 4:
                resultado = num1 / num2;
                operacion = "DIVISIÓN";
                break;
            case 5:
                resultado = num1 % num2;
                operacion = "MÓDULO";
                break;
        }
        
        // Mostrar el resultado centrado en la página
        var resultadoDiv = document.createElement("div");
        resultadoDiv.textContent = operacion + ": " + resultado;
        resultadoDiv.style.textAlign = "center"; // Centrar el texto
        resultadosDiv.appendChild(resultadoDiv);
    }
    
    
}

